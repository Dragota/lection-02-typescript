import { createElement } from '../../helpers/domHelper';

interface modalParams {
  title: string,
  bodyElement: HTMLDivElement,
  onClose: () => void 
}

export function showModal({ title, bodyElement, onClose = () => {} }: modalParams) {
  const root: HTMLDivElement | null = getModalContainer();
  const modal: HTMLDivElement = createModal({ title, bodyElement, onClose }); 
  
  root?.append(modal);
}

function getModalContainer() {
  return document.getElementById('root') as HTMLDivElement;
}

function createModal({ title, bodyElement, onClose }: modalParams): HTMLDivElement {
  const layer: HTMLDivElement = createElement<HTMLDivElement>({ tagName: 'div', className: 'modal-layer' });
  const modalContainer: HTMLDivElement = createElement<HTMLDivElement>({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(title, onClose);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string, onClose: () => void): HTMLDivElement {
  const headerElement: HTMLDivElement = createElement<HTMLDivElement>({ tagName: 'div', className: 'modal-header' });
  const titleElement: HTMLSpanElement = createElement<HTMLSpanElement>({ tagName: 'span' });
  const closeButton: HTMLDivElement = createElement<HTMLDivElement>({ tagName: 'div', className: 'close-btn' });
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  
  const close = () => {
    hideModal();
    onClose();
  }
  closeButton.addEventListener('click', close);
  headerElement.append(title, closeButton);
  
  return headerElement;
}

function hideModal(): void {
  const modal: HTMLDivElement =<HTMLDivElement> document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
