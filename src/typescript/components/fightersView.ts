import { createElement } from '../helpers/domHelper';
import { createFightersSelector, selectFighterFunc } from './fighterSelector';
import { Fighter } from '../models/fighter.model';

export function createFighters(fighters: Partial<Fighter>[]): HTMLDivElement {
  const selectFighter: selectFighterFunc = createFightersSelector();
  const container: HTMLDivElement = createElement<HTMLDivElement>({ tagName: 'div', className: 'fighters___root' });
  const preview: HTMLDivElement = createElement<HTMLDivElement>({ tagName: 'div', className: 'preview-container___root' });
  const fightersList: HTMLDivElement = createElement<HTMLDivElement>({ tagName: 'div', className: 'fighters___list' });
  const fighterElements: HTMLDivElement[] = fighters.map((fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(fighter: Partial<Fighter>, selectFighter: selectFighterFunc): HTMLDivElement {
  const fighterElement: HTMLDivElement = createElement({ tagName: 'div', className: 'fighters___fighter' });
  const imageElement: HTMLImageElement = createImage(fighter);
  const onClick = (event: MouseEvent) => fighter._id && selectFighter(event, fighter?._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter: Partial<Fighter>) {
  const { source = '', name = '' } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement: HTMLImageElement = createElement<HTMLImageElement>({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes
  });

  return imgElement;
}