export interface createElementInterface {
  tagName: string,
  className?: string,
  attributes?: {
    [key: string]: string
  }
}

export function createElement<T extends HTMLElement>({ tagName, className, attributes = {} }: createElementInterface): T {
  const element: T = <T>document.createElement(tagName);

  if (className) {
    const classNames: string[] = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}
