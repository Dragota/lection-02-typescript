import { fightersDetails, fighters } from './mockData';
import { Fighter } from '../models/fighter.model';

const API_URL: string = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI: boolean = true;

type requestMethod = 'GET' | 'POST'| 'PUT'| 'PATCH' | 'DELETE';

interface requestOptions {
  method: requestMethod
}

async function callApi<T>(endpoint: string, method: requestMethod): Promise<T> {
  const url: string = API_URL + endpoint;
  const options: requestOptions = {
    method
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .then((result) => JSON.parse(atob(result.content)))
        .catch((error) => {
          throw error;
        });
}

async function fakeCallApi(endpoint: string): Promise<Fighter | Partial<Fighter>[]> {
  const response: Fighter | Partial<Fighter>[] = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint: string): Fighter {
  const start = endpoint.lastIndexOf('/');
  const end = endpoint.lastIndexOf('.json');
  const id = endpoint.substring(start + 1, end);

  return fightersDetails.find((it) => it._id === id) as Fighter;
}

export { callApi };
